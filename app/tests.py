from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
# from app.views import index
# from app.models import TopLiked
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest

# Create your tests here.
class StoryUnitTest(TestCase):
    def test_story_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    # def test_story_using_index_func(self):
    #     found = resolve('/')
    #     self.assertEqual(found.func, index)

    def test_story_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def test_story10_url(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

    def test_story10_sign_up_and_check_redirect(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        self.assertIn('You are not logged in', selenium.page_source)
        signup = selenium.find_element_by_id('signup')
        signup.click()
        selenium.implicitly_wait(10)
        username = selenium.find_element_by_id('id_username')
        username.send_keys("selenium_gitlab")
        username = selenium.find_element_by_id('id_first_name')
        username.send_keys("varok")
        username = selenium.find_element_by_id('id_last_name')
        username.send_keys("saurfang")
        password1 = selenium.find_element_by_id('id_password1')
        password1.send_keys("worldofwarcrafttheburningcrusade")
        password2 = selenium.find_element_by_id('id_password2')
        password2.send_keys("worldofwarcrafttheburningcrusade")
        email = selenium.find_element_by_id('id_email')
        email.send_keys("ppwppwppw@gmail.com")
        button = selenium.find_element_by_id('submit')
        button.click()
        selenium.implicitly_wait(10)
        self.assertIn('Log in', selenium.page_source)

        # test_story10_log_in_and_check_welcome
        username = selenium.find_element_by_id('id_username')
        username.send_keys("selenium_gitlab")
        password = selenium.find_element_by_id('id_password')
        password.send_keys("worldofwarcrafttheburningcrusade")
        button = selenium.find_element_by_id('submit')
        button.click()
        selenium.implicitly_wait(10)
        self.assertIn('Hi, selenium_gitlab', selenium.page_source)

        #test_story10_log_out_and_check_main_page
        logout = selenium.find_element_by_id('logout')
        logout.click()
        selenium.implicitly_wait(10)
        self.assertIn('You are not logged in', selenium.page_source)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()